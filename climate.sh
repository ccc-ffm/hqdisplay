#!/usr/bin/env bash

humitempfile="/dev/shm/humitemp.log"

humitemp="$(head -n 1 $humitempfile)"

humidity="$(awk '{ print $2 }' <<< "$humitemp")"
temperature="$(awk '{ print $3 }' <<< "$humitemp")"
timestamp="$(date +'%s')"

mosquitto_pub -h strom.ccc-ffm.de --psk 4a4e1c01e4a2641b3905272685b20d99 --psk-identity hq -t /hq/climate/lounge/temperature -m "$timestamp;$temperature" -u hq -P hq -r
mosquitto_pub -h strom.ccc-ffm.de --psk 4a4e1c01e4a2641b3905272685b20d99 --psk-identity hq -t /hq/climate/lounge/humidity -m "$timestamp;$humidity" -u hq -P hq -r
