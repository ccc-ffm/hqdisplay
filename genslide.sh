#!/bin/bash

# Add to /etc/rc.local:
# git clone git://git.ccc-ffm.de/hqdisplay.git /dev/shm/hqdisplay && bash /dev/shm/hqdisplay/runslides.sh &

mydir="$( cd "$(dirname "${0}")" &> /dev/null; pwd )"
fbicmd="fbi -noreadahead -cachemem 0 -noverbose -vt 1 -timeout 3 -blend 500"

# Authenticate against https://strom.ccc-ffm.de/ with RPi serial
rpisn="$(while read line; do case "${line}" in Serial*) echo ${line#*: }; ;; esac; done < /proc/cpuinfo)"
rpisnhash="$(echo -n "${rpisn}" | sha512sum | cut -d' ' -f1)"
## Update Webservers .htpasswd:
# echo htpasswd .htpasswd "$rpisnhash" "$rpisn"

slidesdir="/dev/shm/slides"
mkdir -p "${slidesdir}"

# git pull if repo was never or not updated in the last 10 minutes
if [ ! -f "${mydir}/.git/FETCH_HEAD" -o -n "$(find "${mydir}/.git/FETCH_HEAD" -mmin +10 2>/dev/null)" ]
then
  (
    cd "${mydir}"
    LANG=C git pull | fgrep -q 'Already up-to-date.'
    exit $?
  )
  if [ $? -ne 0 ]
  then
    # something happend in the repo; kill fbi, clean up & restart
    pkill fbi
    ${fbicmd} -once "${mydir}/static/testcard.png"
    find "${slidesdir}" -name '*.png' | xargs --no-run-if-empty rm
    exec bash "${0}" # expect ${0} to reside on an noexec-FS.
  fi
fi

function numpath {
  printf '%s%02d%s' "${slidesdir}/slide" ${1} '.png'
}

function tmpwriter {
  tmpslide=$(TMPDIR="${slidesdir}" mktemp)
  cat > "${tmpslide}"
  mv "${tmpslide}" "${1}"
}

for static in "${mydir}/static/slide"*'.png'
do
  tmpwriter "${slidesdir}/${static##*/}" < "${static}"
done

export LANG='de_DE.UTF-8'

## SLIDE 0: static (ccc-ffm Logo)

## SLIDE 1
test -f "$(numpath 1)" || convert -background black -fill '#00ff00' -pointsize 64 -gravity center label:'Willkommen im\nHackquarter\ndes\nChaos Computer Club\nFrankfurt' png:- | tmpwriter "$(numpath 1)"

## SLIDE 2: static (Club Mate)

## SLIDE 3
test -f "$(numpath 3)" || convert -background black -fill '#e49341' -pointsize 64 -gravity center label:'Durst?\n\nWir haben\nClub Mate\nund weitere Getränke.\n\nFrage ein Mitglied.' png:- | tmpwriter "$(numpath 3)"

## SLIDE 4: static (power distribution)

## SLIDE 5; see below

## SLIDE 6
curl -sk "https://${rpisnhash}:${rpisn}@strom.ccc-ffm.de/powerdisplay.png" | tmpwriter "$(numpath 6)"

## SLIDE 7: static (E-Mail & Twitter)

## SLIDE 8
test -f "$(numpath 8)" || convert -background black -fill '#ff6600' -pointsize 64 -gravity center label:'Mailingliste, IRC\nhttp://ccc-ffm.de/\n\nTwitter\n@cccffm' png:- | tmpwriter "$(numpath 8)"

## SLIDE 9; see below

## SLIDE 10
test -f "$(numpath 10)" || convert -background black -fill '#ff6600' -pointsize 64 -gravity center label:'Jeden 2. Montag\nim Monat:\nBasteln für Freifunk\nhier im HQ!' png:- | tmpwriter "$(numpath 10)"

## SLIDE 11
test -f "$(numpath 11)" || convert -background black -fill '#ff6600' -pointsize 64 -gravity center label:'Jeden 3. Mittwoch\nim Monat:\nPython User Group\nFrankfurt\nhier im HQ!' png:- | tmpwriter "$(numpath 11)"

## SLIDE 12
test -f "$(numpath 12)" || convert -background black -fill '#ff6600' -pointsize 64 -gravity center label:${RANDOM}'\n'${RANDOM}'\n'${RANDOM}'\n'${RANDOM}'\n'${RANDOM} png:- | tmpwriter "$(numpath 12)"


## SLIDE 9
## Always generate temperatue & humidity slide
hass_token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI5Njg0N2ZlNGIwMjQ0MWViYTc2ZTRiZWViZDRjZDAxNCIsImlhdCI6MTU1MzQzNDIxNCwiZXhwIjoxODY4Nzk0MjE0fQ.1fTNXVp55LqLziBZRP0LgMWk8bZhVReHHCJwMSgevR0"
temp="$(curl -s -H "Authorization: Bearer ${hass_token}" 'http://hub.cccffm.space/api/states/sensor.temperature_lounge' | python -c 'import json; import sys; print json.loads(sys.stdin.read())["state"]')"
humid="$(curl -s -H "Authorization: Bearer ${hass_token}" 'http://hub.cccffm.space/api/states/sensor.humidity_lounge' | python -c 'import json; import sys; print json.loads(sys.stdin.read())["state"]')"
convert -background black -fill '#ff6600' -pointsize 64 -gravity center label:'Temperatur: '"${temp}°C"'\nrel. Feuchte: '"${humid}%" png:- | tmpwriter "$(numpath 9)"
echo $(date +%s) $humitemp | tmpwriter "/dev/shm/humitemp.log"

## SLIDE 5
## Always generate slide with power usage
powerusage="$(curl -sk "https://${rpisnhash}:${rpisn}@strom.ccc-ffm.de/power.log" | cut -d';' -f2)"
# save power usage for other tools...
echo "${powerusage}" | tmpwriter "/dev/shm/power.log"
convert -background black -fill '#ff0000' -pointsize 64 -gravity center label:'Aktuelle\nLeistungsaufnahme\ndes HQ:\n\n'"${powerusage}"' Watt' png:- | tmpwriter "$(numpath 5)"

pgrep fbi &>/dev/null || ${fbicmd} "${slidesdir}/slide"*".png"
