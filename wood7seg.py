#!/usr/bin/env python2

from RPi import GPIO
import time
import threading

class Wood7Seg(object):
  digit = [['A', 'B', 'C', 'D', 'E', 'F'],
           ['B', 'C'],
           ['A', 'B', 'G', 'E', 'D'],
           ['A', 'B', 'G', 'C', 'D'],
           ['F', 'G', 'B', 'C'],
           ['A', 'F', 'G', 'C', 'D'],
           ['A', 'F', 'E', 'D', 'C', 'G'],
           ['A', 'B', 'C'],
           ['A', 'B', 'C', 'D', 'E', 'F', 'G'],
           ['G', 'F', 'A', 'B', 'C', 'D']
          ]

  pins = [7, 15, 11, 13]

  seg_hour = {'D': 8,
              'E': 10,
              'F': 12,
              'A': 16,
              'B': 18,
              'C': 22,
              'G': 24,
              'dot': 26
             }

  seg_min = {'A': 8,
             'B': 10,
             'C': 12,
             'D': 16,
             'E': 18,
             'F': 22,
             'G': 24,
             'dot': 26
            }

  def __init__(self, pin):
    self.pin = self.pins[pin]
    self.seg = self.seg_min
    self._value = None
    self._dot = False
    self._seg = None
    if pin <= 1:
      self.seg = self.seg_hour

  def setup(self):
    GPIO.setup(self.pin, GPIO.OUT)
    GPIO.output(self.pin, GPIO.LOW)
    for pin in self.seg.values():
      GPIO.setup(pin, GPIO.OUT)
      GPIO.output(pin, GPIO.HIGH)

  def show(self):
    for pin in self.pins:
      GPIO.output(pin, GPIO.LOW)
    GPIO.output(self.pin, GPIO.HIGH)
    if self._value or self._value == 0:
      for pin in self.digit[self._value]:
        GPIO.output(self.seg[pin], GPIO.LOW)
    if self._dot:
      GPIO.output(self.seg['dot'], GPIO.LOW)
    if self._seg:
      GPIO.output(self.seg[self._seg], GPIO.LOW)

  def reset(self):
    for pin in self.seg.values():
      GPIO.output(pin, GPIO.HIGH)

  def setDigit(self, value):
    self._value = value

  def setSeg(self, value):
    self._seg = value

  def dot(self):
    self._dot = not self._dot


class Woodi(threading.Thread):
  def __init__(self, wait=0.003):
    super(Woodi, self).__init__()
    self._run = True
    self.wait = wait
    self.digit = []
    GPIO.setmode(GPIO.BOARD)
    for i in range(4):
      self.digit.append(Wood7Seg(i))
      self.digit[i].setup()

  def set(self, value):
    lead_zero = True
    for i in range(4):
      nvalue = int((value / (10 ** (3-i))) % 10)
      if lead_zero:
        if nvalue != 0:
          lead_zero = False
        elif i != 3:
          nvalue = None
      self.digit[i].setDigit(nvalue)

  def run(self):
    while self._run:
      for element in self.digit:
        element.reset()
        element.show()
        time.sleep(self.wait)

  def dot(self):
    self.digit[1].dot()
    self.digit[2].dot()

  def cleanup(self):
    GPIO.cleanup()


if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('action')
  args = parser.parse_args()
  woodi = Woodi()
  woodi.set(8888)
  woodi.start()
  try:
    if args.action == 'clock':
      from datetime import datetime
      while True:
        clock = datetime.utcnow()
        showclock = int('%02d%02d' % (clock.hour, clock.minute))
        woodi.set(showclock)
        woodi.dot()
        time.sleep(1)
    elif args.action == 'cpu':
      import psutil
      while True:
        woodi.set(psutil.cpu_percent(interval=1))
    elif args.action == 'power':
      while True:
        with open('/dev/shm/power.log', 'r') as fh:
          try:
            woodi.set(int(fh.read().split('.')[0]))
          except:
            pass
        time.sleep(1)
    elif args.action == 'null':
      while True:
        woodi.set(000)
        time.sleep(1)
    else:
      while True:
        woodi.set(int(args.action))
        time.sleep(1)
  except KeyboardInterrupt:
    print('aus')
  except Exception as exc:
    print(exc)
  finally:
    woodi._run = False
    woodi.join()
    woodi.cleanup()
