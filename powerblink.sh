#!/bin/bash

function blink {
  for v in {1..61..5} {60..0..5}
  do
    case "${1}" in
      red)
        espcol="$((v+10))0000"
        echo ${v} > /sys/bus/usb/drivers/usbled/*/red
        echo 0 > /sys/bus/usb/drivers/usbled/*/green
        echo 0 > /sys/bus/usb/drivers/usbled/*/blue
        ;;
      green)
        espcol="00$((v+10))00"
        echo 0 > /sys/bus/usb/drivers/usbled/*/red
        echo ${v} > /sys/bus/usb/drivers/usbled/*/green
        echo 0 > /sys/bus/usb/drivers/usbled/*/blue
        ;;
      blue)
        espcol="0000$((v+10))"
        echo 0 > /sys/bus/usb/drivers/usbled/*/red
        echo 0 > /sys/bus/usb/drivers/usbled/*/green
        echo ${v} > /sys/bus/usb/drivers/usbled/*/blue
        ;;
      yellow)
        espcol="$((v+10))$((v+10))00"
        echo ${v} > /sys/bus/usb/drivers/usbled/*/red
        echo ${v} > /sys/bus/usb/drivers/usbled/*/green
        echo 0 > /sys/bus/usb/drivers/usbled/*/blue
        ;;
    esac
    timeout -s KILL 2 curl -s "http://esp_9b8df9.hq/control?mode=3&led=0&color=$espcol" &> /dev/null
    sleep 0.1
  done
}

# 'reset' all colors
blink red
blink green
blink blue

while true
do
  # wait for watt
  if [ ! -f /dev/shm/power.log ]
  then
    sleep 1
    continue
  fi
  # /dev/shm/power.log: 330.0
  IFS=. read -a pa < /dev/shm/power.log
  p="${pa[0]}"
  if [ -z "${p}" ]
  then
    sleep 1
    continue
  fi
  if [ "${p}" -lt 80 ]
  then
    sleep 10
  elif [ "${p}" -lt 230 ]
  then
    blink blue
  elif [ "${p}" -lt 500 ]
  then
    blink green
  elif [ "${p}" -lt 1000 ]
  then
    blink yellow
  else
    blink red
  fi
done
